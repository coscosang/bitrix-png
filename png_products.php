<?
/* Изменить на свои */
$IBLOCK_ID = 1;
$TYPE = "catalog";
/*------------------*/

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$arResult = array();
/// Увеличивает все товары на процент
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");



$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", 'PREVIEW_PICTURE', 'DETAIL_PICTURE');
$arFilter = Array("IBLOCK_ID"=> 42, 'INCLUDE_SUBSECTIONS'=>'Y');
$resp = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

$arProducts = [];

function checkPng($imgId){
    if(empty($imgId)) return false;
    $rsFile = CFile::GetByID($imgId);
    $arFile = $rsFile->Fetch();
    if($arFile['CONTENT_TYPE'] == 'image/png' && $arFile['FILE_SIZE'] > 40000){
        return $arFile['SUBDIR'] . $arFile['FILE_NAME'];
    }
    return false;
}

while($obDell = $resp->GetNextElement())
{
    $arFields = $obDell->GetFields();
    //print_r($arFields);
    $arFields['PREVIEW_PICTURE_png'] = checkPng($arFields['PREVIEW_PICTURE']);
    $arFields['DETAIL_PICTURE_png'] = checkPng($arFields['DETAIL_PICTURE']);
    if($arFields['PREVIEW_PICTURE_png'] || $arFields['DETAIL_PICTURE_png']){
        $arProducts[] = $arFields;
    }
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Ссылка</th>
            <th scope="col">Анонс</th>
            <th scope="col">Детальная</th>
        </tr>
        </thead>
        <tbody>

<?
$i = 0;
foreach ($arProducts as $arProduct) { $i++;?>
        <tr>
            <th scope="row"><?=$i?></th>
            <td>
                <a class="text-white" target="_blank" href="/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=42&type=aspro_next_content&lang=ru&ID=<?=$arProduct['ID']?>"><?=$arProduct['NAME']?></a>
            </td>
            <td><?=($arProduct['PREVIEW_PICTURE_png']) ? 'Да' : 'Нет'?></td>
            <td><?=($arProduct['DETAIL_PICTURE_png']) ? 'Да' : 'Нет'?></td>
        </tr>
<?}?>
        </tbody>
    </table>

